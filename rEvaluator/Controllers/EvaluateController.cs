﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RDotNet;
using rEvaluator.Models;
using rEvaluator.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rEvaluator.Controllers
{
    [Route("api/evaluate")]
    [ApiController]
    public class EvaluateController : ControllerBase
    {
        private readonly RServiceBuilder _rServiceBuilder;
        public EvaluateController(RServiceBuilder rServiceBuilder) {
            _rServiceBuilder = rServiceBuilder;
        }

        private List<string> CopyFiles(UploadFiles files)
        {
            string dir = Directory.GetCurrentDirectory();
            List<string> filesPath = new List<string>();
            foreach (var file in files.Files)
            {
                string fpath = String.Format("{0}{1}{2}", dir, Path.DirectorySeparatorChar, file.FileName);
                filesPath.Add(fpath);

                FileStream fstream = new FileStream(fpath, FileMode.Create);
                file.CopyTo(fstream);
                fstream.Close();
            }

            return filesPath;
        }

        private void DeleteFiles(List<string> files)
        {
            foreach (var fpath in files)
            {
                System.IO.File.Delete(fpath);
            }
        }

        // POST api/<EvaluateController>
        [HttpPost("plots")]
        public FileStreamResult GetPlots([FromForm] UploadFiles files)
        {
            List<string> copiedFiles = this.CopyFiles(files);

            string tempFile = String.Format("{0}/plots.pdf", Path.GetTempPath());
            _ = _rServiceBuilder
                .StartPdf(tempFile)
                .Evaluate(files.Main, files.InputData, out _)
                .StopPdf()
                .Clear();

            this.DeleteFiles(copiedFiles);

            Stream fileStream = new FileStream(tempFile, FileMode.Open);
            return File(fileStream, "application/octet-stream");
        }

        [HttpPost]
        public Dictionary<string, string> Get([FromForm] UploadFiles files) {
            List<string> copiedFilese = this.CopyFiles(files);

            _ = _rServiceBuilder
                .DisablePlots("pdf")
                .Evaluate(files.Main, files.InputData, out Dictionary<string, string> outputs)
                .StopPdf()
                .Clear();

            this.DeleteFiles(copiedFilese);
            return outputs;
        }
    }
}
