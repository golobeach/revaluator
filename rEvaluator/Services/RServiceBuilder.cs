﻿using Microsoft.AspNetCore.Http;
using RDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rEvaluator.Services
{
    public class RServiceBuilder
    {
        private readonly REngine _engine;
        public RServiceBuilder()
        {
            _engine = REngine.GetInstance();
        }

        public RServiceBuilder Evaluate(IFormFile file, Dictionary<string, object> input, out Dictionary<string, string> output)
        {
            using (var stream = file.OpenReadStream())
            {
                _engine.Evaluate(stream);

                StringBuilder sb = new StringBuilder();
                foreach(var entry in input)
                {
                    if ((entry.Value is int) || (entry.Value is float) || (entry.Value is double) || (entry.Value is long) ) {
                        sb.AppendFormat("\"{0}\" = {1},", entry.Key, entry.Value);
                    } else {
                        sb.AppendFormat("\"{0}\" = \"{1}\",", entry.Key, entry.Value);
                    }
                }
                sb.Remove(sb.Length - 1, 1);


                string functionCall = String.Format("output <- main(list({0}))", sb.ToString());

                var evaluatedData = _engine.Evaluate(functionCall);

                output = new Dictionary<string, string>();
                foreach (var data in evaluatedData.AsList().ToPairlist())
                {
                    string value = _engine.Evaluate(String.Format("output${0}", data.PrintName)).AsCharacter()[0];
                    output.Add(data.PrintName, value);
                }
            }
            return this;
        }

        private RServiceBuilder StartOutput(string type, string fname)
        {
            string data = String.Format("{0}('{1}')", type, fname.Replace("\\", "\\\\"));
            _engine.Evaluate(data);
            return this;
        }

        private RServiceBuilder DevOff()
        {
            _engine.Evaluate("dev.off()");
            return this;
        }

        public RServiceBuilder StartPng(string fname)
        {
            return this.StartOutput("png", fname);
        }

        public RServiceBuilder StartPdf(string fname)
        {
            return this.StartOutput("pdf", fname);
        }

        public RServiceBuilder DisablePlots(string type) {
            _engine.Evaluate(String.Format("{0}(file = NULL)", type));
            return this;
        }

        public RServiceBuilder StopPdf()
        {
            return this.DevOff();
        }

        public RServiceBuilder StopPng()
        {
            return this.DevOff();
        }

        public RServiceBuilder Clear()
        {
            _engine.ClearGlobalEnvironment();
            return this;
        }
    }
}
