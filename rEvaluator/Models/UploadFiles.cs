﻿using BrunoZell.ModelBinding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rEvaluator.Models
{
    public class UploadFiles
    {
        public IFormFile Main { get; set; }
        public IFormFile[] Files { get; set; }

        [ModelBinder(BinderType = typeof(JsonModelBinder))]
        public Dictionary<string, object> InputData { get; set; }
    }
}
